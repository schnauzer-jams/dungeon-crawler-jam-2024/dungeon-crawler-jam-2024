# Dungeon Crawler Jam 2024

## Post apocolyptic crawler

Dungeon crawler where you capture spirits with instant film to use in battle. Manage an limited size album of spirits and different stat boosting film types in order to delve deeper into a post apocolyptic shopping mall.